#pragma once

#include "GamePanel.hpp"
#include "Snake.hpp"
#include "Apple.hpp"

class Controller
{
	private:
	Snake* snake;
	GamePanel* gamePanel;
	void update();
	bool check();
	Apple *apple;
	void moveApple();

	public:
	Controller(Snake *s, GamePanel *gp, Apple* a);
	void play();
};
