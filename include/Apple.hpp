#pragma once
#include "Point.hpp"

class Apple{
	private:
	Point position;
	char visual;

	public:
	Apple();
	Apple(Point p);
	Point getPosition();
	void setPosition(Point p);
	char getVisual();
};
