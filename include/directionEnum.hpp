#pragma once
enum class Direction {
	Up,
	Down,
	Left,
	Right
};

#define DEFAULT Direction::Up
