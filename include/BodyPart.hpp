#pragma once
#include "Point.hpp"
#include "directionEnum.hpp"

class BodyPart {
	private:
	Point position;
	char visual;
	Point next;
	Point last;

	public:
	BodyPart(Point p, Point n, Point l, char v);
	Point getNext();
	Point getLast();
	char getVisual();
	Point getPosition();
	Direction opposite(Direction dir);
	void move(Point newNext);
};
