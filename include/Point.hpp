#pragma once
#include "directionEnum.hpp"

class Point
{

        public:
        int x,y;
        Point();
        Point(int x, int y);
	void set(int a, int b);
	Point oppositePoint(Point a);
	static Direction opposite(Direction dir);
	static Point findPointByDir(Point p, Direction dir);
	static bool isSame(Point p1, Point p2);
};
