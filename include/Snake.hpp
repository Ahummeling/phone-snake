#pragma once
#include <vector>

#include "Point.hpp"
#include "directionEnum.hpp"
#include "BodyPart.hpp"

class Snake
{
	private:
	int length;
	std::vector<BodyPart> body;
	Direction curr;

	public:
	int xsize;
	int ysize;
	Snake(int w, int h);
 	~Snake();
	void createHead(Point position);
	void grow();
	void grow(BodyPart origin);
	int getLength();
	void move();
	bool hitSelf();
	bool isSnake(Point p);
	bool isOutOfBounds();
	bool isOnApple(Point p);
	std::vector<BodyPart> getBody();
};
