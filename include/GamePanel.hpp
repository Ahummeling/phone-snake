#pragma once
#include "Rectangle.hpp"
#include "Snake.hpp"
#include "Apple.hpp"

class GamePanel
{
	private:
	int height;
	int width;
	Rectangle rect;
	Snake *snakePointer;
	char** map;
	Apple *apple;

	public:
	GamePanel(int h, int w, Snake *s, Apple *a);
	~GamePanel();
	void draw();
	void drawField();
	void drawSnake();
	void drawApple();
	bool isSnake(Point p);
	void clearScreen();
};
