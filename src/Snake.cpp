#include "Snake.hpp"
#include "BodyPart.hpp"
#include "Point.hpp"

Snake::Snake(int w, int h)
{
	int slength = 3;
	curr = DEFAULT;
	Point start(5,5);
	createHead(start);
	for(int i = 1; i<slength; i++){
		grow(body[i-1]);
	}
	xsize = w-1;
	ysize = h-1;
}

void Snake::createHead(Point position)
{
	Point next = Point::findPointByDir(position,curr);
	Point last = position.oppositePoint(next);
	BodyPart part(position, next, last, '@');
	body.push_back(part);
	length = 1;
}

void Snake::grow()
{
	grow(body.back());
}

void Snake::grow(BodyPart origin)
{
	length++;
	Point n = origin.getPosition();
	Point p = origin.getLast();
	Point l = p.oppositePoint(n);
	BodyPart part(p, n, l, '$');
	body.push_back(part);
}

int Snake::getLength()
{
	return length;
}

void Snake::move()
{
	Point n =  body[0].getNext();
	Point nn = Point::findPointByDir(n,curr);
	body[0].move(nn);
	for(int i=1;i<body.size();i++){
		Point newNext = body[i-1].getPosition();
		body[i].move(newNext);
	}
}

std::vector<BodyPart> Snake::getBody()
{
	return body;
}

bool Snake::isOutOfBounds()
{
	BodyPart head = body[0];
	Point p = head.getPosition();
	if((p.x)%xsize && (p.y)%ysize) {
		printf("snake at %i,%i not out of boundaries %i,%i\n",p.x,p.y,xsize,ysize);
		return false;
	}
	printf("snake at %i,%i out of boundaries %i,%i\n",p.x,p.y,xsize,ysize);
	return true;
}

bool Snake::hitSelf()
{
	Point head = body[0].getPosition();
	return isSnake(head);
}

bool Snake::isSnake(Point p)
{
        for (int i = 1; i < body.size(); i++) {
		Point pos = body[i].getPosition();
                if(Point::isSame(p,pos)){
                        return true;
                }
        }
        return false;
}

bool Snake::isOnApple(Point apple)
{
	Point head = body[0].getPosition();
	return Point::isSame(apple,head);
}

Snake::~Snake()
{
	body.clear();
}
