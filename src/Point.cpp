#include <iostream>

#include "directionEnum.hpp"
#include "Point.hpp"

Point::Point()
{
	Point(0,0);
}

Point::Point(int a, int b)
{
	x = a;
	y = b;
}

void Point::set(int a, int b)
{
	x = a;
	y = b;
}

bool Point::isSame(Point p1, Point p2)
{
	int ret = !((p1.x-p2.x) || (p1.y-p2.y));
	return ret == 0 ? false : true;
}

Point Point::findPointByDir(Point p, Direction dir)
{
	int x = p.x;
	int y = p.y;
	switch(dir) {
		case Direction::Up:
			y++;
			break;
		case Direction::Down:
			y--;
			break;
		case Direction::Right:
			x++;
			break;
		case Direction::Left:
			x--;
			break;
		default:
			return findPointByDir(p,DEFAULT);
	}
	Point newPoint(x,y);
	return newPoint;
}

Direction Point::opposite(Direction dir)
{
        switch(dir) {
                case Direction::Up:
			return Direction::Down;
                case Direction::Down:
			return Direction::Up;
                case Direction::Right:
			return Direction::Left;
                case Direction::Left: 
			return Direction::Right;
                default: return opposite(DEFAULT);
	}
}

Point Point::oppositePoint(Point a)
{
	Point b;
	b.x = 2*x-a.x;
	b.y = 2*y-a.y;
	return b;
}
