#include <iostream>
#include "android/input.h"
#include "android/keycodes.h"

#include "GamePanel.hpp"
#include "Rectangle.hpp"
#include "Snake.hpp"
#include "Controller.hpp"

int main()
{
	std::cout << "Snake!" << std::endl;
	int w = 44;
	int h = 21;
	Snake s(h,w);
	Apple a;
	GamePanel gp(w,h,&s,&a);
	Controller c(&s,&gp,&a);
	AInputEvent aie;
	int32_t device = AInputEvent_getDeviceId(&aie);
	std::cout << device << std::endl;
	return 0;
	c.play();
	return 0;
}
