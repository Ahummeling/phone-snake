#include <iostream>
#include <chrono>
#include <thread>
#include <cstdlib>

#include "Controller.hpp"
#include "Snake.hpp"
#include "GamePanel.hpp"
#include "Apple.hpp"

Controller::Controller(Snake *s, GamePanel *gp, Apple *a):
snake(s), gamePanel(gp), apple(a)
{
	moveApple();
}

void Controller::play()
{
	bool game = true;
	while(game){
		snake->move();
		update();
		game = check();
		std::this_thread::sleep_for(std::chrono::milliseconds(250));
	}
	std::cout << "Game over!" << std::endl;
	std::cin >> game;
}

bool Controller::check()
{
	if(snake->isOutOfBounds() || snake->hitSelf()) return false;
	if(snake->isOnApple(apple->getPosition())) {
		snake->grow();
		moveApple();
	}
	return true;
}

void Controller::moveApple()
{
	int x = (1+rand())%snake->xsize;
	int y = (1+rand())%snake->ysize;
	Point p(x,y);
	apple->setPosition(p);
}

void Controller::update()
{
	gamePanel->clearScreen();
	gamePanel->draw();
}
