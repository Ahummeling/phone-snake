#include <iostream>

#include "GamePanel.hpp"
#include "Rectangle.hpp"
#include "Point.hpp"
#include "BodyPart.hpp"
#include "Apple.hpp"

GamePanel::GamePanel(int h, int w, Snake *s, Apple* a) :
height(h), width(w), snakePointer(s), apple(a)
{
	rect.setSize(h,w);
	map = new char*[w];
	for (int i = 0; i < w; i++) {
		map[i] = new char[h];
	}
}


void GamePanel::draw()
{
	drawField();
	drawSnake();
	drawApple();
	for(int x = 0; x < width; x++){
		for(int y = 0; y<height; y++) {
			std::cout << map[x][y];
		}
		std::cout << std::endl;
	}
}

void GamePanel::drawField()
{
	for(int i = 0; i < rect.w; i++){
		for(int j = 0; j < rect.h; j++){
			char output =(i%(rect.w-1))*(j%(rect.h-1)) ? ' ' : '#';
			map[i][j] = output;
		}
	}
}

void GamePanel::drawSnake()
{
	std::vector<BodyPart> set = snakePointer->getBody();
	for(int i =0;i<set.size();i++){
		Point p = set[i].getPosition();
		char v = set[i].getVisual();
		map[p.x][p.y] = v;

	}
}

void GamePanel::clearScreen()
{
	for(int i = 0; i <25; i++){
		std::cout << std::endl;
	}
}

bool GamePanel::isSnake(Point p)
{
	std::vector<BodyPart> set = snakePointer->getBody();
	for (int i = 0; i < set.size(); i++) {
		Point pos = set[i].getPosition();
		if(Point::isSame(p,pos)){

			return true;
		}
	}
	return false;
}

void GamePanel::drawApple()
{
	Point p = apple->getPosition();
	map[p.x][p.y] = apple->getVisual();
}

GamePanel::~GamePanel()
{
	delete[] map;
}
