#include "BodyPart.hpp"
#include "Point.hpp"
#include "directionEnum.hpp"

BodyPart::BodyPart(Point p, Point n, Point l, char v)
{
	position = p;
	next = n;
	last = l;
	visual = v;
}

Point BodyPart::getNext()
{
	return next;
}

Point BodyPart::getLast()
{
	return last;
}

char BodyPart::getVisual()
{
	return visual;
}

void BodyPart::move(Point newNext)
{
	last = position;
	position = next;
	next = newNext;
}

Direction BodyPart::opposite(Direction dir)
{
	switch(dir) {
		case Direction::Up: return Direction::Down;
		case Direction::Down: return Direction::Up;
		case Direction::Right: return Direction::Left;
		case Direction::Left: return Direction::Right;
		default: return opposite(DEFAULT);
	}
}

Point BodyPart::getPosition()
{
	return position;
}
