#include "Apple.hpp"
#include "Point.hpp"
Apple::Apple()
{
	visual = '&';
}

Apple::Apple(Point p):
position(p)
{
	visual = '&';
}

Point Apple::getPosition()
{
	return position;
}
void Apple::setPosition(Point p)
{
	position = p;
}

char Apple::getVisual()
{
	return visual;
}
